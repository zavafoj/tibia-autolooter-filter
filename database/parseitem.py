
# Copyright 2016 Mark Raasveldt
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import math
import re
import urllib.request
import sqlite3
import time

lookRegex = re.compile('(<div id="twbox-look">[^\n]*)')
htmlTagRegex = re.compile('<[^>]*>')
numberRegex = re.compile('([0-9]+[,.]?[0-9]*[,.]?[0-9]*[,.]?[0-9]*[,.]?[0-9]*)')
imageRegex = re.compile('<a href="([^"]*)"[ \t\n]*class="image image-thumbnail"')
imageRegex2 = re.compile('src="([^"]*vignette[^"]*)"')

from imageoperations import properly_crop_item
from urlhelpers import getImage

def saveImage(tibia_id, image, prefix):
    path = '{}/images/{}.gif'.format(prefix, tibia_id)
    print(path)
    f = open(path, 'wb')
    f.write(image)
    f.close()

def getURL(url, decode=False):
    url = url.replace('ñ', '%C3%B1')
    try:
        response = urllib.request.urlopen(url)
    except:
        return None
    if decode:
        result = response.read().decode('utf-8')
    else:
        result = response.read()
    return result

def parseItem(title, attributes, prefix, categories):

    item = {
        'id': -1,
        'name': '',
        'value': 0,
        'stackable': False,
        'weight': 0.0,
        'ratio': 0.0,
        'category': ''
    }

    #Handle if item is dropped

    if 'droppedby' in attributes:
        splits = attributes['droppedby'].replace('Dropped By', '').replace('{', '').replace('}', '').split('|')
        splits = list(filter(None, splits))
        if (len(splits) < 1):
            print ("Item not dropped by any monster")
            return None

    else:
        print ("Item not dropped by any monster")
        return None

    #Handle if item has ID
    if 'itemid' in attributes:
        splits = attributes['itemid'].split(',')
        for ident in splits:
            try:
                item['id'] = int(ident.strip())
            except:
                print ("Item Has no TibiaId")
                return None
    else:
        print ("Item Has no TibiaId")
        return None

    # Handle item name
    item['name'] = title
    if 'actualname' in attributes and len(attributes['actualname']) > 0:
        item['name'] = attributes['actualname']

    # Handle item value
    npcValue = None
    if 'npcvalue' in attributes:
        try:
            npcValue = int(attributes['npcvalue'])
        except: pass
    if (npcValue == None or npcValue == 0) and 'npcprice' in attributes:
        try: npcValue = int(attributes['npcprice'])
        except: pass

    actualValue = None
    if 'value' in attributes:
        match = numberRegex.search(attributes['value'])
        if match != None:
            actualValue = int(match.groups()[0].replace(',','').replace('.',''))

    npcPrice = None
    if 'npcprice' in attributes:
        try:
            npcPrice = int(attributes['npcprice'])
            if actualValue != None and npcPrice > 0 and actualValue > npcPrice:
                actualValue = npcPrice
        except:
            pass
    if npcValue != None and (actualValue == None or npcValue > actualValue):
        actualValue = npcValue

    item['value'] = actualValue

    # Handle item weight
    capacity = None
    if 'weight' in attributes:
        try: capacity = float(attributes['weight'])
        except: pass

    item['weight'] = capacity


    if 'stackable' in attributes:
        item['stackable'] = 'yes' in attributes['stackable'].strip().lower()

    category = None
    if 'primarytype' in attributes:
        category = attributes['primarytype']
    elif 'itemclass' in attributes:
        category = attributes['itemclass']

    item['category'] = category
    if category not in categories:
        categories.append(category)


    # tibia wiki uses some function to get the image url rather than storing it explicitly, and I don't really want to bother to decipher it
    url = "http://tibia.wikia.com/wiki/%s" % (title.replace(' ', '_'))
    itemHTML = getURL(url, True)
    if itemHTML == None: return None
    image = getImage(url, getURL, imageRegex, properly_crop_item)
    if image == None or image == False:
        url = "http://tibia.wikia.com/wiki/File:%s.gif" % (title.replace(' ', '_'))
        image = getImage(url, getURL, imageRegex2, properly_crop_item)
        if image == None or image == False:
            print('failed to get image for item', title)
        else:
            saveImage(item['id'], image, prefix)
    else:
        saveImage(item['id'], image, prefix)

    gold_ratio = max(0 if npcValue == None else npcValue, 0 if actualValue == None else actualValue) / (1 if capacity == None or capacity == 0 else capacity)
    item['ratio'] = round(gold_ratio, 2)

    return item
