#!/usr/bin/python3.5

import pickle
import urllib.request
import sqlite3
import re
import xml.etree.ElementTree
import sys
import json

from parseitem import parseItem

tag_prefix = '{http://www.mediawiki.org/xml/export-0.10/}'

def getTag(tag): return tag_prefix + tag

def formatTitle(title):
	return title.replace('&#39;', "'").replace('#39;', "'").replace('<br>', ' ').replace('<Br>', ' ').replace('<BR>', ' ')

def wordCount(input_string, word):
    return sum(1 for _ in re.finditer(r'%s' % re.escape(word), input_string))

def parseAttributes(content):
    attributes = dict()
    depth = 0
    parseValue = False
    attribute = ""
    value = ""
    for i in range(len(content)):
        if content[i] == '{' or content[i] == '[':
            depth += 1
            if depth >= 3:
                if parseValue:
                    value = value + content[i]
                else:
                    attribute = attribute + content[i]
        elif content[i] == '}' or content[i] == ']':
            if depth >= 3:
                if parseValue:
                    value = value + content[i]
                else:
                    attribute = attribute + content[i]
            if depth == 2:
                attributes[attribute.strip()] = value.strip()
                parseValue = False
                attribute = ""
                value = ""
            depth -= 1
        elif content[i] == '=' and depth == 2:
            parseValue = True
        elif content[i] == '|' and depth == 2:
            attributes[attribute.strip()] = value.strip()
            parseValue = False
            attribute = ""
            value = ""
        elif parseValue:
            value = value + content[i]
        else:
            attribute = attribute + content[i]
    return attributes

def saveDatabase(items, categories, dumpdir) :
	with open(dumpdir+'/itembase.json', 'w') as fp:
		json.dump({'items': items, 'categories': categories}, fp)

if __name__ == '__main__':
    root = xml.etree.ElementTree.parse(sys.argv[1]).getroot()
    categories=[]
    items=[]

    for child in root.getchildren():
        titleTag = child.find(getTag('title'))
        if titleTag == None: continue
        title = titleTag.text
        if 'help:' in title.lower() or 'talk:' in title.lower() or 'template:' in title.lower() or 'updates/' in title.lower() or 'user:' in title.lower() or 'loot/' in title.lower() or 'tibiawiki:' in title.lower(): continue
        title = formatTitle(title)
        revisionTag = child.find(getTag('revision'))
        if revisionTag == None: continue
        textTag = revisionTag.find(getTag('text'))
        if textTag == None: continue
        content = textTag.text
        if content == None: continue
        attributes = parseAttributes(content)
        lcontent = content.lower()
        if wordCount(lcontent, '{{infobox item') == 1 or wordCount(lcontent, '{{infobox_item') == 1:
            print('Item: ', title)
            item = parseItem(title, attributes, sys.argv[2], categories)
            if item:
                print(item)
                items.append(item)

    saveDatabase(items, categories, sys.argv[2])
    print ("Done")
