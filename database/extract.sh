#!/bin/bash

DUMP_FILE=tibiawiki_pages_current.xml
EXPORT_DIR=$PWD/../src/assets

if [ "$1" != "--dry-run" ]; then

	if [ -d "dump" ]; then
		echo "The dump directory exist. Deleting..."
		rm -Rf dump
	fi

	mkdir dump

	if [ -d "../assets" ]; then
		echo "The assets directory exist. Deleting..."
		rm -Rf ../assets
	fi

	mkdir ../src/assets
	mkdir ../src/assets/images

	if [ -f $DUMP_FILE ]; then
		echo "Old tibiawiki dump exists. Deleting..."
		rm $DUMP_FILE
	fi

	wget http://s3.amazonaws.com/wikia_xml_dumps/t/ti/$DUMP_FILE.7z
	7z x $DUMP_FILE.7z -o./dump
	rm $DUMP_FILE.7z

fi

python3.5 parseXML.py $PWD/dump/$DUMP_FILE $EXPORT_DIR
