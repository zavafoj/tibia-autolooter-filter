import { Component, ViewChild, Input } from '@angular/core';
import { PageEvent } from '@angular/material';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/combineLatest';

import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

import { Item } from '../../item';

import { ItemFilter } from '../../item-filer';
import { ItemService } from '../../services/item.service';

interface ItemListFilter {
  name: string,
  options: ItemFilter,
  categories: string[]
};

@Component({
  selector: 'app-itemlist',
  templateUrl: './itemlist.component.html',
  styleUrls: ['./itemlist.component.css']
})
export class ItemlistComponent {
  public displayedColumns: string[] = [
    'image', 'name', 'value', 'weight',
    'ratio', 'stackable', 'category'
  ];
  private selectedCategories: string[] = [];
  private filterOptions: ItemFilter = {
    maximumPrice: 0,
    maximumGpOzRate: 0,
    minimumOz: 0,
    isStackable: ""
  }

  private items: Item[];
  private selection = new SelectionModel<number>(true, []);
  private filterString: string = "";

  public dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private itemService: ItemService) {}

  ngAfterViewInit() {
    this.itemService.getItems().subscribe(items => {
      this.items = items;
      this.dataSource = new MatTableDataSource(this.items);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      this.dataSource._filterData = function(data: any[]) {
        // If there is a filter string, filter out data that does not contain it.
        // Each data object is converted to a string using the function defined by filterTermAccessor.
        // May be overriden for customization.
        this.filteredData = data.filter(obj => this.filterPredicate(obj, this.filter));

        if (this.paginator) { this._updatePaginator(this.filteredData.length); }

        return this.filteredData;
      }

      this.dataSource.filterPredicate = (item:Item, filter: string) => {
        this.filterString = filter;

        if (this.selectedCategories.length) {
          if (this.selectedCategories.indexOf(item.category) === -1) {
            return false;
          }
        }

        if (this.filterOptions.maximumPrice) {
          if (this.filterOptions.maximumPrice < item.value) {
            return false;
          }
        }

        if (this.filterOptions.maximumGpOzRate) {
          if (this.filterOptions.maximumGpOzRate < item.ratio) {
            return false;
          }
        }

        if (this.filterOptions.minimumOz) {
          if (this.filterOptions.minimumOz > item.weight) {
            return false;
          }
        }

        if (this.filterOptions.isStackable) {
          if (item.stackable && this.filterOptions.isStackable === "No") {
            return false;
          } else if (!item.stackable && this.filterOptions.isStackable === "Yes") {
            return false;
          }
        }

        return item.name.indexOf(filter.toLowerCase()) !== -1;
      }
    });

    this.itemService.getSelectedCategories().subscribe(categories => {
      this.selectedCategories = categories;
      this.applyFilter(this.filterString)
    });

    this.itemService.getSelectedItems().subscribe(items => {
      this.selection.clear();
      items.forEach(item => {
        this.selection.select(item);
      });
    });

    this.itemService.getFilterOptions().subscribe(filter => {
      this.filterOptions = filter;
      this.applyFilter(this.filterString);
    })
  }

  selectItem(id: number) {
    this.selection.toggle(id);
    this.itemService.changeItemSelection(this.selection.selected);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.paginator.pageIndex = 0;
  }
}
