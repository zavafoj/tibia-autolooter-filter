import { Component, ViewChild, OnInit, Input } from '@angular/core';
import { ItemService } from '../../services/item.service';

class Blacklist {
  blacklistTypes: number[] = [];
  listType: string = "blacklist";
  whitelistTypes: number[] = [];

  constructor (blacklist: number[]) {
    this.blacklistTypes = blacklist;
  }
}

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.css']
})
export class DownloadComponent implements OnInit {

  @Input() blacklistText: string = this.getJson([]);

  getJson(items: number[]) : string{
    return JSON.stringify(new Blacklist(items), null, 4);
  }

  constructor(private itemService: ItemService) { }

  ngOnInit() {
    this.itemService.getSelectedItems().subscribe(items => {
      this.blacklistText = this.getJson(items);
    })
  }

  downloadFile() {
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(this.blacklistText);
    var downloadAnchorNode = document.createElement('a');
    downloadAnchorNode.setAttribute("href", dataStr);
    downloadAnchorNode.setAttribute("download", "lootBlackWhiteList.json");
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
  }

}
