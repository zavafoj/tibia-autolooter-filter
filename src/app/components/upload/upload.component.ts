import { Component, OnInit, ViewChild, Input, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ItemService } from '../../services/item.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  @ViewChild('file') file;

  @Input() fileName: string = "";
  @Input() placeholder: string = "";

  private handlePlaceholder (filename: string) {
    this.fileName = filename;
    this.placeholder = filename ? "Selected file: " : "No file chosen";
  }

  private handleFile (inputValue: any) {
    let self = this;

    const file:File = inputValue.files[0];
    const reader:FileReader = new FileReader();

    reader.onloadend = function (e) {
      try {
        const configuration = JSON.parse(reader.result);
        const blacklist = configuration.blacklistTypes;

        if (Array.isArray(blacklist)) {
          self.itemService.changeItemSelection(blacklist);
          self.handlePlaceholder(file.name);
        } else {
          throw new Error("Please select correct 'lootBlackWhiteList.json' file")
        }

      } catch (e) {
        self.dialog.open(UploadComponentAlertDialog, {
          data: { title: "Error", content: e.toString() }
        })
      }
    };
    reader.readAsText(file);
  }

  constructor(private itemService: ItemService, public dialog: MatDialog) {}

  ngOnInit() {
    this.handlePlaceholder("");
  }

  handleFileUpload($event) : void {
    try {
      this.handleFile($event.target);
    } catch (e) { }
  }

  buttonUploadClicked() {
    let event = new MouseEvent('click', { bubbles: true });
    this.file.nativeElement.dispatchEvent(event);
  }

  buttonClearClicked() {
    this.itemService.changeItemSelection([]);
    this.handlePlaceholder("");
  }
}

@Component({
  template: `
    <h2 mat-dialog-title>{{ data.title }}</h2>

    <mat-dialog-content class="">
      <p>{{ data.content }}</p>
    </mat-dialog-content>

    <mat-dialog-actions>
      <button mat-button mat-dialog-close>Close</button>
    </mat-dialog-actions>
  `
})
export class UploadComponentAlertDialog {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}
}
