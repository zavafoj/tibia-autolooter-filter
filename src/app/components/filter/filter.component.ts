import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ItemService } from '../../services/item.service';
import { ItemFilter } from '../../item-filer';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent{
  options: FormGroup;

  constructor(fb: FormBuilder, private itemService: ItemService) {
    this.options = fb.group({
        'maximumPrice': [0, Validators.min(0)],
        'maximumGpOzRate': [0, Validators.min(0)],
        'minimumOz': [0, Validators.min(0)],
        'isStackable': ""
    });
  }

  applyFilter() {
    this.itemService.changeFilterOptions({
      'maximumPrice': this.options.value.maximumPrice,
      'maximumGpOzRate': this.options.value.maximumGpOzRate,
      'minimumOz': this.options.value.minimumOz,
      'isStackable': this.options.value.isStackable
    })
  }

}
