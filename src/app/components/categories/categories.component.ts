import { Component } from '@angular/core';

import { ItemService } from '../../services/item.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent {
  categories:string[];
  selectedCategories:string[] = [];

  private filter(category: string) {
    return category ? category : "No Category";
  }

  constructor(private itemService: ItemService) {}

  ngAfterViewInit() {
    this.itemService.getCategories()
      .subscribe(categories => this.categories = categories);
  }

  selectCategory(category: string) {
    let categoryIndex = this.selectedCategories.indexOf(category);
    if (categoryIndex !== -1) {
      this.selectedCategories.splice(categoryIndex, 1);
    } else {
      this.selectedCategories.push(category);
    }

    this.itemService.changeCategorySelection(this.selectedCategories);

  }

}
