// Angular Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// In-app Modules
import { MaterialModule } from './material.module';

// In-app Components
import { AppComponent } from './app.component';
import { ItemlistComponent } from './components/itemlist/itemlist.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { FilterComponent } from './components/filter/filter.component';
import { UploadComponent, UploadComponentAlertDialog } from './components/upload/upload.component';
import { DownloadComponent } from './components/download/download.component';
import { ManageComponent } from './components/manage/manage.component';

// In-app Services
import { ItemService } from './services/item.service';

@NgModule({
  declarations: [
    AppComponent,
    ItemlistComponent,
    CategoriesComponent,
    FilterComponent,
    UploadComponent,
    DownloadComponent,
    UploadComponentAlertDialog,
    ManageComponent
  ],
  entryComponents: [
    UploadComponentAlertDialog
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ItemService],
  bootstrap: [AppComponent]
})
export class AppModule { }
