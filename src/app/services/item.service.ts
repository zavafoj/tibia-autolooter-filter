import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Item } from '../item';
import { ItemFilter } from '../item-filer';

@Injectable()
export class ItemService {
  private itembaseUrl = 'assets/itembase.json';
  private categoriesUrl = 'assets/categories.json';

  private categorySubject = new Subject<string[]>();
  private selectedItemSubject = new Subject<number[]>();
  private filtersSubject = new Subject<ItemFilter>();

  constructor(private http: Http) { }

  getItems(): Observable<Item[]> {
    return this.http.get(this.itembaseUrl)
      .map(response => response.json())
      .catch(err => Observable.throw(err));
  }

  getCategories(): Observable<string[]> {
    return this.http.get(this.categoriesUrl)
      .map(response => response.json())
      .catch(err => Observable.throw(err));
  }

  getSelectedCategories(): Observable<string[]> {
    return this.categorySubject.asObservable();
  }

  changeCategorySelection(categories: string[]) {
    this.categorySubject.next(categories);
  }

  getSelectedItems(): Observable<number[]> {
    return this.selectedItemSubject.asObservable();
  }

  changeItemSelection(items: number[]) {
    this.selectedItemSubject.next(items);
  }

  getFilterOptions(): Observable<ItemFilter> {
    return this.filtersSubject.asObservable();
  }

  changeFilterOptions(filters: ItemFilter) {
    this.filtersSubject.next(filters)
  }

}
