export interface Item {
  id: number;
  name: string;
  category: string;
  value: number;
  weight: number;
  ratio: number;
  stackable: boolean;
}
