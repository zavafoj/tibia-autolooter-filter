export interface ItemFilter {
  maximumPrice: number;
  maximumGpOzRate: number;
  minimumOz: number;
  isStackable: string;
}
